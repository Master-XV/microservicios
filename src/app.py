import sqlite3
import requests
from flask import Flask, request,jsonify

app = Flask(__name__)

@app.route('/netflix/original-content', methods=['GET', 'POST'])
def crud():
    if request.method == 'GET':
        sqlite_conn = get_database_connection('./original_content.db')
        sqlite_cursor = sqlite_conn.cursor()
        if request.args.get('genre'):
            genre = request.args.get('genre')
            query = "SELECT * FROM original_content WHERE genre='%s'" % genre
            sqlite_cursor.execute(query)
            data = sqlite_cursor.fetchall()
            result = convert_cursor_to_json(data)
            return jsonify(result)
        if request.args.get('type'):
            ctype = request.args.get('type')
            query = "SELECT * FROM original_content WHERE type='%s'" % ctype
            sqlite_cursor.execute(query)
            data = sqlite_cursor.fetchall()
            result = convert_cursor_to_json(data)
            return jsonify(result)
        if request.args.get('rating'):
            rating = request.args.get('rating')
            query = "SELECT * FROM original_content WHERE rating='%s'" % rating
            sqlite_cursor.execute(query)
            data = sqlite_cursor.fetchall()
            result = convert_cursor_to_json(data)
            return jsonify(result)
        else:
            sqlite_cursor.execute('SELECT * FROM original_content')
            data = sqlite_cursor.fetchall()
            result = convert_cursor_to_json(data)
            return jsonify(result)
    if request.method == 'POST':
        sqlite_conn = get_database_connection('./original_content.db')
        sqlite_cursor = sqlite_conn.cursor()
        title = request.json['name']
        url = 'http://www.omdbapi.com/'
        payload={'apikey':'98e8a5ed','t':title}
        result = requests.get(url,params=payload)
        rjson=result.json()
        name = rjson['Title']
        genre = rjson['Genre']
        ctype = rjson['Type']
        imdb_rating = rjson['imdbRating']
        query = "INSERT INTO original_content (name,type,genre,imdb_rating) VALUES('%s','%s','%s','%s')" % (name,ctype,genre,imdb_rating)
        sqlite_cursor.execute(query)
        data = sqlite_conn.commit()
        return jsonify({'message':'Success'})
    
@app.route('/netflix/original-content/<int:id>', methods=['GET', 'PATCH'])
def crudById(id):
    sqlite_conn = get_database_connection('./original_content.db')
    sqlite_cursor = sqlite_conn.cursor()
    if request.method=='GET':
        query = "SELECT * FROM original_content WHERE id='%s'" % id
        sqlite_cursor.execute(query)
        data = sqlite_cursor.fetchall()
        result = convert_cursor_to_json(data)
        return jsonify(result)
    if request.method=='PATCH':
        name = request.json['name']
        genre = request.json['genre']
        ctype = request.json['type']
        imdb_rating = request.json['imdb_rating']
        query = "UPDATE original_content SET name='%s', type='%s', genre='%s', imdb_rating='%s' WHERE id='%s'" % (name,ctype,genre,imdb_rating,id)
        sqlite_cursor.execute(query)
        data = sqlite_conn.commit()
        return jsonify({'message':'Success'})
    
def get_database_connection(database_path):
    conn = sqlite3.connect(database_path)
    return conn

def convert_cursor_to_json(cursor_data):
    result_list = []
    for e in cursor_data:
        temp_dict = {}
        temp_dict['id'] = e[0]
        temp_dict['name'] = e[1]
        temp_dict['type'] = e[2]
        temp_dict['genre'] = e[3]
        temp_dict['imdb_rating'] = e[4]
        result_list.append(temp_dict)
    return result_list

if __name__ == '__main__':
    app.run(debug=True)
