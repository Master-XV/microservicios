from microservicios import db, ma  # __main__


class Original_content(db.Model):
    id = db.Column(db.Integer, primary_key=True,
                   autoincrement=True, unique=True)
    name = db.Column(db.Text, nullable=False)
    type = db.Column(db.Text, nullable=False)
    genre = db.Column(db.Text, nullable=False)
    imdb_rating = db.Column(db.Float)

    def __init__(self, name, type, genre, imdb_rating):
        self.name = name
        self.type = type
        self.genre = genre
        self.imdb_rating = imdb_rating


db.create_all()


class Original_Schema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'type', 'genre', 'imdb_rating')


content = Original_Schema()
manyContent = Original_Schema(many=True)
